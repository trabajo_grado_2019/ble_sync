/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
 *
 * @defgroup ble_sdk_app_template_main main.c
 * @{
 * @ingroup ble_sdk_app_template
 * @brief Template project main file.
 *
 * This file contains a template for creating a new application. It has the code necessary to wakeup
 * from button, advertise, get a connection restart advertising on disconnect and if no new
 * connection created go back to system-off mode.
 * It can easily be used as a starting point for creating a new application, the comments identified
 * with 'YOUR_JOB' indicates where and how you can customize.
 */

/********************************************************************************************************
																					Samuel Escobar Gonzalez SEG
Template and example taken from the following website: https://github.com/bjornspockeli/custom_ble_service_example

Modiified to add diferent characterisitcs that each correspond to a signal obtained from the body.

The custom UUID key was obtained from https://www.uuidgenerator.net/version4 and the UUID key generated is :
8b9e4b58-d24e-42b8-ae83-ff9a5a37d0a0 of in HEX values: {0xA0, 0xD0, 0x37, 0x5A, 0x9A, 0xFF, 0x83, 0xAE, 0xB8, 0x42, 0x4E, 0xD2, 0x58, 0x4B, 0x9E, 0x8B}
																					


*/



#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "fds.h"
#include "peer_manager.h"
#include "bsp_btn_ble.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "ble_cus.h"

//--------------------------------------------------------------
//          Additional includes CAT
//-------------------------------------------------------------

//  otros
#include "Functions_SEG.h"

#include "nrf_gpiote.h"
#include "nrf_ppi.h"
#include "time_sync.h"
#include "app_util_platform.h"

//  adc
#include "nrf_drv_saadc.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf_ppi.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"

//  rtc
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"

//  ADS
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "bsp.h"

//  twi
#include "nrf_drv_twi.h"

//  interrupts
#include "nrf_drv_gpiote.h"

//   spi
#include "nrf_drv_spi.h"

//  uart
#include "app_uart.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif


//--------------------------------------------------------------
//          END ADDITIONAL INCLUDES CAT
//-------------------------------------------------------------


#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2    /**< Reply when unsupported features are requested. */

#define DEVICE_NAME                     "PNS"                       /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "Kiron"                   /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                300                                     /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                     /**< The advertising timeout in units of seconds. */

#define APP_BLE_OBSERVER_PRIO           1                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                       /**< A tag identifying the SoftDevice BLE configuration. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(15, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(15, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                   0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define NOTIFICATION_INTERVAL           APP_TIMER_TICKS(15)     

#define SEC_PARAM_BOND                  1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */


//----------------------------------------------------------------------------------------------------------------
//          ADDITIONAL DEFINES CAT
//----------------------------------------------------------------------------------------------------------------

//CAT  -  saadc
#define SAMPLES_IN_BUFFER 1

//CAT  -  ads
#define MAX_TEST_DATA_BYTES     (15U)                /**< max number of test bytes to be used for tx and rx. */

//SEG - RTC implementation
#define COMPARE_COUNTERTIME  (3UL)    
const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2); /**< Declaring an instance of nrf_drv_rtc for RTC0. */

//SEG - UART Disable Hardware Flow Control
#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED

//SPI imported defined
#define SPI_INSTANCE  2 /**< SPI instance index. */ //SEG - SPI intance 0 and TWI intance 1 cannot be used together. 
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

#define RESET_COUNTER       1000//Counter in ms to ensure that if the system doesnt captures a sample in less than RESET_COUNTER the system should restart. 
#define SYSTEM_RESET_TIME   1200000 //Time in ms to restart the system 20 minutes

#define ECG_ACCUM_BUFFER_SIZE 5 //SEG - size of buffer to accumulate ADS samples, it must be a multiple of 3 to store timeStamp and ECG data. 

#define UART_TX_BUF_SIZE    256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE    256 
#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED

#define NODE_IDENTIFIER     0   //CAT - byte de identificación si es nodo derecho (1) o izquierdo (0)

//----------------------------------------------------------------------------------------------------------------
//          END DEFINES
//----------------------------------------------------------------------------------------------------------------


NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
BLE_CUS_DEF(m_cus);
BLE_ADVERTISING_DEF(m_advertising);                                             /**< Advertising module instance. */

APP_TIMER_DEF(m_notification_timer_id);

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                        /**< Handle of the current connection. */


//----------------------------------------------------------------------------------------------------------------
//          VARIABLES CAT
//----------------------------------------------------------------------------------------------------------------

//  saadc
static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(0);
static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];
static nrf_ppi_channel_t     m_ppi_channel;
static uint32_t              m_adc_evt_counter;

uint32_t timeStamp = 0;         //Variable to store the time stamp of the system. 

uint32_t adsData[3] = {0};

uint16_t ecgResetCounter = 0; 

uint32_t systemResetCounter = 0;

uint8_t negativeEcgData[2]={0x01,0x01};

uint32_t adsStorage[ECG_ACCUM_BUFFER_SIZE][2] = {0};

uint32_t* adsStoragePntr;

uint8_t adsCounter = 0;

uint32_t max_time = 0x41893;

//App flags handler
typedef struct segFlags{
	bool dataRdyADS;        //DataRdy flag is signaling that the ADS1292R has finished 1 conversion.
	bool finishedInit;	//Flag to indicate that the starting setup was executed.
	bool executeADS;	//Execute ADS routine, this means that the AFE setup was done correctly.
	bool systemReset;
        bool notifyECG;           //Flag to indicate connected and notifying.
}segFlags;

struct segFlags flagsSEG; //SEG- initialize the flag structure. 

uint8_t ble_data_pckt1[20] = {0};
uint8_t ble_data_pckt2[20] = {0};

uint8_t dummyData[20] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
	

//----------------------------------------------------------------------------------------------------------------
//          END VARIABLES
//----------------------------------------------------------------------------------------------------------------


static ble_uuid_t m_adv_uuids[] =                                   /**< Universally unique service identifiers. */
{
    {BLE_UUID_HEART_RATE_SERVICE,           BLE_UUID_TYPE_BLE},
    {BLE_UUID_DEVICE_INFORMATION_SERVICE,   BLE_UUID_TYPE_BLE}
};


//----------------------------------------------------------------------------------------------------------------------
//          ADITIONAL FUNCTIONS CAT
//----------------------------------------------------------------------------------------------------------------------

//-------------- IMPORTED ADC FUNCTIONS---------------------------------------------------------------------------------

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {

    }
}


void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);
}

//--------------END ADC FUNCTIONS-------------------------------------------------------------------------------------------


//------------- IMPORTED RTC FUNCTIONS-------------------------------------------------------------------------------------------

static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    ret_code_t err_code;
    if(flagsSEG.notifyECG == true)
    {
        //printf("Sample\n\r");
       
        if (int_type == NRF_DRV_RTC_INT_COMPARE0)
        {
       
        }
        else if (int_type == NRF_DRV_RTC_INT_TICK)
        {

        }
    }
}

static void lfclk_config(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_clock_lfclk_request(NULL);
}


static void rtc_config(void)
{
    uint32_t err_code;

    //Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = 130; //CAT -- prescaler 32.78 / 1 + prescaler = F muestreo en KHz
    config.interrupt_priority = 3;
    err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
    APP_ERROR_CHECK(err_code);

    //Enable tick event & interrupt
    nrf_drv_rtc_tick_enable(&rtc,true);

    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
    //SEG- Disable Countertime interrupts. Work only with Tick events.
    err_code = nrf_drv_rtc_cc_set(&rtc,0,COMPARE_COUNTERTIME * 8,false); 
    APP_ERROR_CHECK(err_code);

    //Power on RTC instance
    nrf_drv_rtc_enable(&rtc);
}

//--------------------------END RTC FUNCTIONS-------------------------------------------------------------------------


//--------------------------IMPORTED GPIO FUNCTIONS-------------------------------------------------------------------------

void int_pin_ADS1292R_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    flagsSEG.dataRdyADS = true;
}

static void gpio_init(void)
{
  //SEG - Set up the HW interrupts to be executed when a pin changes state.   
    ret_code_t err_code;
	

    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
    in_config.pull = NRF_GPIO_PIN_PULLUP;

    err_code = nrf_drv_gpiote_in_init(11, &in_config, int_pin_ADS1292R_handler);
	
    APP_ERROR_CHECK(err_code);

	
    //nrf_drv_gpiote_in_event_enable(11, true);
}

//--------------------------END GPIO FUNCTIONS-------------------------------------------------------------------------


//--------------------------IMPORTED SPI FUNCTIONS-------------------------------------------------------------------------

void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
	spi_xfer_done = true;
}


void spi_init(void){

nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = 15; //SS or CS
    spi_config.miso_pin = 12; //MISO
    spi_config.mosi_pin = 14; //MOSI
    spi_config.sck_pin  = 13; //SCLK
		spi_config.mode = NRF_DRV_SPI_MODE_1;//SEG
		spi_config.frequency =  NRF_DRV_SPI_FREQ_500K;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, NULL, NULL)); //SEG - If SPI handler needed add it here. 
	
}

//--------------------------END SPI FUNCTIONS-------------------------------------------------------------------------


//-----------------------IMPORTED UART FUNCTIONS-------------------------------------------------------------------------

void uart_event_handle(app_uart_evt_t * p_event)
{
    uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}


static void uart_init(void)
{
    uint32_t                     err_code;

    //CAT - comm params para las tarjetas azules
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
        .baud_rate    = NRF_UART_BAUDRATE_115200
    };

    //CAT - comm params para las tarjetas moradas
//    app_uart_comm_params_t const comm_params =
//    {
//        .rx_pin_no    = 23,    //RX in OpenLog
//        .tx_pin_no    = 24,    //TX in OpenLog
//        .rts_pin_no   = 27,    //RTS
//        .cts_pin_no   = 20,   //CTS 
//        .flow_control = UART_HWFC,
//        .use_parity   = false,
//        .baud_rate    = NRF_UART_BAUDRATE_57600
//    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
}

//--------------------------END UART FUNCTIONS-----------------------------------------------------------------------


//--------------------IMPORTED SYNC FUNCTIONS-----------------------------------------------------------------------

static void sync_timer_button_init(void)
{
    uint32_t       err_code;
    uint8_t        rf_address[5] = {0xDE, 0xAD, 0xBE, 0xEF, 0x19};
    ts_params_t    ts_params;

    nrf_ppi_channel_endpoint_setup(
        NRF_PPI_CHANNEL0, 
        (uint32_t) nrf_timer_event_address_get(NRF_TIMER3, NRF_TIMER_EVENT_COMPARE4),
        nrf_gpiote_task_addr_get(NRF_GPIOTE_TASKS_OUT_3));
    nrf_ppi_channel_enable(NRF_PPI_CHANNEL0);
    
    ts_params.high_freq_timer[0] = NRF_TIMER3;
    ts_params.high_freq_timer[1] = NRF_TIMER2;
    ts_params.rtc             = NRF_RTC1;
    ts_params.egu             = NRF_EGU3;
    ts_params.egu_irq_type    = SWI3_EGU3_IRQn;
    ts_params.ppi_chg         = 0;
    ts_params.ppi_chns[0]     = 1;
    ts_params.ppi_chns[1]     = 2;
    ts_params.ppi_chns[2]     = 3;
    ts_params.ppi_chns[3]     = 4;
    ts_params.rf_chn          = 125; /* For testing purposes */
    memcpy(ts_params.rf_addr, rf_address, sizeof(rf_address));
    
    err_code = ts_init(&ts_params);
    APP_ERROR_CHECK(err_code);
    
    err_code = ts_enable();
    APP_ERROR_CHECK(err_code);
    
    NRF_LOG_INFO("Started listening for beacons.\r\n");
}

//-------------------------END SYNC FUNCTIONS-----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//            END FUNCTIONS CAT
//----------------------------------------------------------------------------------------------------------------------


/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(bool erase_bonds)
{
    if (erase_bonds == true)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETED_SUCEEDED evetnt
    }
    else
    {
        ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);

        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_INFO("Connected to a previously bonded device.");
        } break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            NRF_LOG_INFO("Connection secured: role: %d, conn_handle: 0x%x, procedure: %d.",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
                  
            //printf("Data start.\n\r");
            //flagsSEG.notifyECG = true;
        } break;

        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
        } break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;

        case PM_EVT_STORAGE_FULL:
        {
            // Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        {
            advertising_start(false);
        } break;

        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
        {
            // The local database has likely changed, send service changed indications.
            pm_local_database_has_changed();
        } break;

        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } break;

        case PM_EVT_PEER_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } break;

        case PM_EVT_PEERS_DELETE_FAILED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        } break;

        case PM_EVT_ERROR_UNEXPECTED:
        {
            // Assert.
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}


/**@brief Function for handling the ecg measurement timer timeout.
 *
 * @details This function will be called each time the ecg measurement timer expires.
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler.
 */
static void notification_timeout_handler(void * p_context)
{
      
    UNUSED_PARAMETER(p_context);
    ret_code_t err_code;

    err_code = ble_cus_custom_value_update(&m_cus, &dummyData);


//    nrf_saadc_value_t sample = 0;
//    nrf_drv_saadc_sample_convert(0, &sample);
//
//    adsStorage[adsCounter][0] = ts_timestamp_get_ticks_u32(0);
//    adsStorage[adsCounter][1] = sample;
//
//    printf("%x\n\r", adsStorage[adsCounter][0]);
//
//    adsCounter++;
//
//    if(adsCounter==1)
//    {
//        ble_data_pckt1[2] = (*(adsStoragePntr+0)>>24);
//        ble_data_pckt1[3] = (*(adsStoragePntr+0)>>16);
//        ble_data_pckt1[4] = (*(adsStoragePntr+0)>>8);
//        ble_data_pckt1[5] = (*(adsStoragePntr+0));
//
//        ble_data_pckt1[6] = (*(adsStoragePntr+1)>>16);
//        ble_data_pckt1[7] = (*(adsStoragePntr+1)>>8);
//        ble_data_pckt1[8] = (*(adsStoragePntr+1));
//    }
//
//    if(adsCounter==2)
//    {
//        ble_data_pckt1[9] = (*(adsStoragePntr+2)>>24);
//        ble_data_pckt1[10] = (*(adsStoragePntr+2)>>16);
//        ble_data_pckt1[11] = (*(adsStoragePntr+2)>>8);
//        ble_data_pckt1[12] = (*(adsStoragePntr+2));
//
//        ble_data_pckt1[13] = (*(adsStoragePntr+3)>>16);
//        ble_data_pckt1[14] = (*(adsStoragePntr+3)>>8);
//        ble_data_pckt1[15] = (*(adsStoragePntr+3));
//    }
//
//    if(adsCounter==3)
//    {
//        ble_data_pckt1[16] = (*(adsStoragePntr+4)>>24);
//        ble_data_pckt1[17] = (*(adsStoragePntr+4)>>16);
//        ble_data_pckt1[18] = (*(adsStoragePntr+4)>>8);
//        ble_data_pckt1[19] = (*(adsStoragePntr+4));
//
//        ble_data_pckt2[16] = (*(adsStoragePntr+5)>>16);
//        ble_data_pckt2[17] = (*(adsStoragePntr+5)>>8);
//        ble_data_pckt2[18] = (*(adsStoragePntr+5));
//    }
//
//    if(adsCounter==4)
//    {
//        ble_data_pckt2[2] = (*(adsStoragePntr+6)>>24);
//        ble_data_pckt2[3] = (*(adsStoragePntr+6)>>16);
//        ble_data_pckt2[4] = (*(adsStoragePntr+6)>>8);
//        ble_data_pckt2[5] = (*(adsStoragePntr+6));
//
//        ble_data_pckt2[6] = (*(adsStoragePntr+7)>>16);
//        ble_data_pckt2[7] = (*(adsStoragePntr+7)>>8);
//        ble_data_pckt2[8] = (*(adsStoragePntr+7));
//    }
//
//    if(adsCounter==5)
//    {
//        ble_data_pckt2[9] = (*(adsStoragePntr+8)>>24);
//        ble_data_pckt2[10] = (*(adsStoragePntr+8)>>16);
//        ble_data_pckt2[11] = (*(adsStoragePntr+8)>>8);
//        ble_data_pckt2[12] = (*(adsStoragePntr+8));
//
//        ble_data_pckt2[13] = (*(adsStoragePntr+9)>>16);
//        ble_data_pckt2[14] = (*(adsStoragePntr+9)>>8);
//        ble_data_pckt2[15] = (*(adsStoragePntr+9));
//    }
//    
//    if(adsCounter>= ECG_ACCUM_BUFFER_SIZE)
//    {
//        adsCounter = 0;
//        err_code = ble_cus_custom_value_update(&m_cus, &ble_data_pckt1);
//        err_code = ble_cus_custom_value_update(&m_cus, &ble_data_pckt2);
//    }

}



/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    // Initialize timer module.
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Create timers.
    err_code = app_timer_create(&m_notification_timer_id, APP_TIMER_MODE_REPEATED, notification_timeout_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
//    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Custom Service Service events.
 *
 * @details This function will be called for all Custom Service events which are passed to
 *          the application.
 *
 * @param[in]   p_cus_service  Custom Service structure.
 * @param[in]   p_evt          Event received from the Custom Service.
 *
 */
static void on_cus_evt(ble_cus_t     * p_cus_service,
                       ble_cus_evt_t * p_evt)
{
    ret_code_t err_code;
    
    switch(p_evt->evt_type)
    {
        case BLE_CUS_EVT_NOTIFICATION_ENABLED:
             printf("Notifications enabled.\n\r");
             //flagsSEG.notifyECG = true;
             err_code = app_timer_start(m_notification_timer_id, NOTIFICATION_INTERVAL, NULL);
             APP_ERROR_CHECK(err_code);
             break;

        case BLE_CUS_EVT_NOTIFICATION_DISABLED:
            printf("Notifications disabled.\n\r");
            err_code = app_timer_stop(m_notification_timer_id);
            APP_ERROR_CHECK(err_code);
            break;
						

        case BLE_CUS_EVT_CONNECTED:
            printf("Connected.\n\r");
            err_code = app_timer_start(m_notification_timer_id, NOTIFICATION_INTERVAL, NULL);
            break;

        case BLE_CUS_EVT_DISCONNECTED:
            printf("Disonnected.\n\r");
            err_code = app_timer_stop(m_notification_timer_id);
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
            /* YOUR_JOB: Add code to initialize the services used by the application.*/
        ret_code_t                         err_code;
        ble_cus_init_t                     cus_init;

         // Initialize CUS Service init structure to zero.
        memset(&cus_init, 0, sizeof(cus_init));
        cus_init.evt_handler                = on_cus_evt;
    
			//SEG_CHAR config the security parameters of the different chars. 
        BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cus_init.custom_value_char_attr_md.cccd_write_perm);
        BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&cus_init.custom_value_char_attr_md.read_perm);
        BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&cus_init.custom_value_char_attr_md.write_perm);


        err_code = ble_cus_init(&m_cus, &cus_init);
        APP_ERROR_CHECK(err_code);	
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    //cp_init.start_on_notify_cccd_handle    = m_cus.custom_value_handles.cccd_handle;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting timers.
 */
static void application_timers_start(void)
{
//    err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
//    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising.");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;

        default:
            break;
    }
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

#if defined(S132)
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
#endif

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated when button is pressed.
 */
static void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break; // BSP_EVENT_SLEEP

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break; // BSP_EVENT_DISCONNECT

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break; // BSP_EVENT_KEY_0

        default:
            break;
    }
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    ret_code_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}



/**@brief Function for application main entry.
 */
int main(void)
{
    //uint32_t err_code;
    bool erase_bonds;

    log_init();
    uart_init();
    gpio_init();
    app_timer_init();

    timers_init();
    buttons_leds_init(&erase_bonds);

    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    conn_params_init();
    peer_manager_init();

    sync_timer_button_init();     // CAT - starts listening for beacons, gpio pin 24 (disabled).

    saadc_init();
    spi_init();
    printf("Inits check\r\n");

    //application_timers_start();

    //CAT - primero inicializar el ADS para darle tiempo para settle.
    adsStoragePntr = &adsStorage[0][0];

    if(ADS1292_init(spi) == NRF_SUCCESS)
    {
        flagsSEG.executeADS = true;
    } else
    {
	flagsSEG.executeADS = false; 
    }														
	
    flagsSEG.finishedInit = true;
    nrf_delay_ms(1000);     // CAT - OJO, los delays pueden tener conflicto con el BLE.

    printf("ADS init check\r\n");

    advertising_init();
    advertising_start(erase_bonds);

    ble_data_pckt1[0] = 1;      //CAT - la primer posicion indica de cual nodo viene el paquete de datos: 1 para derecho, 0 para izquierdo.
    ble_data_pckt2[0] = 1;

    ble_data_pckt1[1] = 1;      //CAT - la segunda posición indica el tipo de paquete. 1 lleva el tiempo del dato del medio, 0 lleva el ecg.
    ble_data_pckt2[1] = 0;

    nrf_drv_gpiote_in_event_enable(11, true);   //CAT- habilitar el pin de toma de datso del ADC cuando todo este inicializado.

    static bool m_send_sync_pkt = false;      //CAT - predeterminado a recibir sync packets, no puede mandarlos. No ts_tx_start.
    ret_code_t err_code;

    flagsSEG.notifyECG = false;       //CAT - asegurarse que no este habilitada la toma de datos hasta que no se establezca la conexión

    printf("Advertising...\r\n");

    //rtc_config();

    // Enter main loop.
    for (;;)
    {
        if(flagsSEG.notifyECG == true)
        {
            if(flagsSEG.dataRdyADS == true)
            {
                flagsSEG.dataRdyADS = false;

                if(ADS1292_ReadData(spi,adsData) != NRF_SUCCESS)
                {
                    printf("ReadData Failed\r\n");
                    flagsSEG.executeADS = false;
                    appendDataADS1292R(&timeStamp,(uint32_t*)negativeEcgData);

                }else
                {
                    //CAT - envio datos en bursts de 5 (ECG accum buffer) datos.
                    adsStorage[adsCounter][0] = ts_timestamp_get_ticks_u32(0);
                    adsStorage[adsCounter][1] = *(adsData+2);
					
                    adsCounter++;
                                        
                    if(adsCounter==1)
                    {
                        ble_data_pckt1[0] = 1;
                        ble_data_pckt1[1] = 1;

                        ble_data_pckt1[2] = (*(adsStoragePntr+0)>>24);
                        ble_data_pckt1[3] = (*(adsStoragePntr+0)>>16);
                        ble_data_pckt1[4] = (*(adsStoragePntr+0)>>8);
                        ble_data_pckt1[5] = (*(adsStoragePntr+0));

                        ble_data_pckt1[6] = (*(adsStoragePntr+1)>>16);
                        ble_data_pckt1[7] = (*(adsStoragePntr+1)>>8);
                        ble_data_pckt1[8] = (*(adsStoragePntr+1));
                    }

                    if(adsCounter==2)
                    {
                        ble_data_pckt1[9] = (*(adsStoragePntr+2)>>24);
                        ble_data_pckt1[10] = (*(adsStoragePntr+2)>>16);
                        ble_data_pckt1[11] = (*(adsStoragePntr+2)>>8);
                        ble_data_pckt1[12] = (*(adsStoragePntr+2));

                        ble_data_pckt1[13] = (*(adsStoragePntr+3)>>16);
                        ble_data_pckt1[14] = (*(adsStoragePntr+3)>>8);
                        ble_data_pckt1[15] = (*(adsStoragePntr+3));
                    }

                    if(adsCounter==3)
                    {
                        ble_data_pckt1[16] = (*(adsStoragePntr+4)>>24);
                        ble_data_pckt1[17] = (*(adsStoragePntr+4)>>16);
                        ble_data_pckt1[18] = (*(adsStoragePntr+4)>>8);
                        ble_data_pckt1[19] = (*(adsStoragePntr+4));

                        ble_data_pckt2[0] = 1;
                        ble_data_pckt2[1] = 1;

                        ble_data_pckt2[16] = (*(adsStoragePntr+5)>>16);
                        ble_data_pckt2[17] = (*(adsStoragePntr+5)>>8);
                        ble_data_pckt2[18] = (*(adsStoragePntr+5));
                    }

                    if(adsCounter==4)
                    {
                        ble_data_pckt2[2] = (*(adsStoragePntr+6)>>24);
                        ble_data_pckt2[3] = (*(adsStoragePntr+6)>>16);
                        ble_data_pckt2[4] = (*(adsStoragePntr+6)>>8);
                        ble_data_pckt2[5] = (*(adsStoragePntr+6));

                        ble_data_pckt2[6] = (*(adsStoragePntr+7)>>16);
                        ble_data_pckt2[7] = (*(adsStoragePntr+7)>>8);
                        ble_data_pckt2[8] = (*(adsStoragePntr+7));
                    }

                    if(adsCounter==5)
                    {
                        ble_data_pckt2[9] = (*(adsStoragePntr+8)>>24);
                        ble_data_pckt2[10] = (*(adsStoragePntr+8)>>16);
                        ble_data_pckt2[11] = (*(adsStoragePntr+8)>>8);
                        ble_data_pckt2[12] = (*(adsStoragePntr+8));

                        ble_data_pckt2[13] = (*(adsStoragePntr+9)>>16);
                        ble_data_pckt2[14] = (*(adsStoragePntr+9)>>8);
                        ble_data_pckt2[15] = (*(adsStoragePntr+9));
                    }
    
                    if(adsCounter>= ECG_ACCUM_BUFFER_SIZE)
                    {
                        adsCounter = 0;
                        err_code = ble_hrs_heart_rate_measurement_send(&m_cus, &ble_data_pckt1);
                        err_code = ble_hrs_heart_rate_measurement_send(&m_cus, &ble_data_pckt2);
                    }
                }
            }
        }
    }
}


/**
 * @}
 */
