






#ifndef FUNCTIONS_SEG
#define FUNCTIONS_SEG

//***********************************************MAX30101 DEFINES**********************************************

//------------------------------------((( MAX30101 Register Config Values ))) ------------------------------------------

#define MAX30101         0x57    //Sensor Adress

#define intStatus 			 0x00 		//Adress:0x00 READ ONLY
#define intStatus2       0x00    //Adress:0x01 READ ONLY
#define intEnable        0x40    //Adress:0x02
#define intEnable2       0x02    //Adress:0x03
#define fifoWritePntr    0x00    //Adress:0x04
#define overFlowCntr     0x00    //Adress:0x05
#define fifoReadPntr     0x00    //Adress:0x06
#define fifoData         0x00    //Adress:0x07
#define fifoConfig       0x11    //Adress:0x08
#define modeConfig       0x07    //Adress:0x09
#define spo2Config       0x49    //Adress:0x0A

#define led1PulseAmp     0x77    //Adress:0x0C
#define led2PulseAmp     0x77    //Adress:0x0D
#define led3PulseAmp     0x77  	 //Adress:0x0E MAKE SURE THE ACTUAL SENSOR IS USED

#define proxMode         0x1F    //Adress:0x10
#define multiLedControl1 0x03    //Adress:0x11
#define multiLedControl2 0x00    //Adress:0x12
#define dieTempInteger   0x00    //Adress:0x1F
#define dieTempfraction  0x00    //Adress:0x20
#define dieTempConfig    0x00    //Adress:0x21
#define proxIntTsHold    0x0F    //Adress:0x30

//***************************END -MAX30101 DEFINES ****************

#include <stdint.h>
#include "nrf_drv_twi.h"
#include "nrf_drv_spi.h"

//*****************************ADS1292R Register and Pin Defines*********************************


//GPIOs for ADS interface. 
#define clkSel     	19
#define pwrDown    	17
#define adsStart   	16 
#define adsDataRdy 	11

//ADS 	System Commands
#define WAKEUP 			0x02
#define STANDBY			0x04
#define RESET                   0x06
#define START			0x08
#define STOP 			0x0A
#define OFFSETCAL		0X1A

//ADS Data Read Commands
#define RDATAC 			0x10
#define SDATAC 			0x11
#define RDATA 			0x12

//ADS Register Read/Write Comands
#define RREG                    0x20		
#define WREG			0x40

//Register Values 
#define ID 			0x73
#define CONFIG1 		0x01
#define CONFIG2 		0xA8
#define LOFF                    0x10
#define CH1SET 			0x40
#define CH2SET	 		0x60
#define RLD_SENS 		0xAC
#define LOFF_SENS               0x00
#define LOFF_STAT               0x00
#define RESP1 			0xF2
#define RESP2 		 	0x83
#define GPIO			0x03

//**************************************END ADS1292R Register and Defines ************************



//*******************************************Functions Declaration***************************************
uint8_t init_OL(void);
uint8_t init_OL_wAppend(void);
void get_msg(uint8_t* pntr, uint8_t chtr);
void send_msg(uint8_t* pntrSend);
void appendDataMAX30101(uint32_t* timeStamp,uint32_t* ppgData);
void appendDataTemp(uint32_t* timeStamp, uint8_t* tempData);
void appendDataAnalogPPG(uint32_t* timeStamp,int16_t* analogPPG);
void appendDataADS1292R(uint32_t* timeStamp,uint32_t* ecgData);
void appendData_APPG_and_ECG(uint32_t* timeStampECG,uint32_t* ecgData,uint32_t* timeStampPPG,int16_t* analogPPG);
void appendTrash(void);
void appendADSBuffer(uint32_t* adsDataBufferPntr);

uint32_t MAX30101_Reboot(nrf_drv_twi_t m_twi,uint8_t* registerRead);
uint32_t MAX30101_Reset(nrf_drv_twi_t m_twi);
uint32_t MAX30101_Register_Setup(nrf_drv_twi_t m_twi);
uint32_t MAX3010_Register_Read(nrf_drv_twi_t m_twi, uint8_t* registerRead);
uint32_t MAX30101_DataGet(nrf_drv_twi_t m_twi, uint32_t* dataPntr);
uint32_t MAX30101_InterruptRead(nrf_drv_twi_t m_twi, uint8_t* interrupts);
uint32_t MAX30101_TempRead(nrf_drv_twi_t m_twi);
uint32_t MAX30101_GetTempData(nrf_drv_twi_t m_twi, uint8_t* tempData);

uint32_t ADS1292_init(nrf_drv_spi_t spi);
uint32_t ADS1292_ReadData(nrf_drv_spi_t spi, uint32_t* adsData);

#endif
