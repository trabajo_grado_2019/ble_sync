
#include "Functions_SEG.h"

#include "app_uart.h"
#include "nrf_uart.h"
#include "nrf_uarte.h"
#include <stdio.h>
#include <string.h>
#include "bsp.h"
#include "nrf_delay.h"


#include "nrf_drv_spi.h"


uint8_t rxBuffer[255] = {0};
char txBuffer[511] = {0};



/***********************************************************
                     OpenLog Functions SEG
***********************************************************/
//void get_msg(uint8_t* pntr, uint8_t chtr){
//	uint8_t counterGet =0;
//	uint8_t flagGet =0;
//	while(flagGet != 1){
//		while (app_uart_get((pntr+counterGet)) != NRF_SUCCESS);
//		if(*(pntr+counterGet)== chtr){
//		flagGet =1;
//		}
//		counterGet++;
//	}
//}

//void send_msg(uint8_t* pntrSend){
//	
//	uint8_t flagSend=0;
//	uint8_t counterSend=0;
//	while(flagSend == 0){
//		
//	while(app_uart_put(*(pntrSend+counterSend)) != NRF_SUCCESS);
//	
//	if(*(pntrSend + counterSend)== 0x0D){
//		flagSend = 1;
//	}	
//		counterSend++;			
//	}
//}

//uint8_t init_OL(void){
//	
//	uint8_t rxOlBuffer[100]={0}; //SEG - Buffer to recieve the incoming data from the OpenLog Module.
// 	uint8_t saveNext[4]={0};//SEG - Variable to store the 3 digit number to be written in the SD card.
//
//	//Reset OpenLog before start.
//	nrf_gpio_cfg_output(7); //SEG- Declare pin 0.07 as output.
//	nrf_gpio_pin_set(7);
//	nrf_delay_ms(100);
//	nrf_gpio_pin_clear(7);
//	nrf_delay_ms(100);
//	nrf_gpio_pin_set(7);
//	
//	//SEG- Wait until less than (<) is recieved signaling system reset.
//	while(rxBuffer[0] != 60){
//		app_uart_get(rxBuffer);
//	}
//	
//	//SEG- Put the OLog in command mode sending 3 times the 26 character. 
//	for(uint8_t i = 0; i<3; i++){
//		app_uart_put(26);
//	}
//	
//	//SEG-Waint until a grater than(>) is recieved in the rxBuffer.
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}	
//	
//	//app_uart_flush(); 
//	//SEG-Read numfile.txt to know which new file to create. 
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"read numfile.txt\r");
//	send_msg((uint8_t*)txBuffer);
//	get_msg(rxOlBuffer, 62);
//	
//	saveNext[0]= (rxOlBuffer[22]-48)*100;
//	saveNext[1]= (rxOlBuffer[23]-48)*10;
//	saveNext[2]= (rxOlBuffer[24]-48);
//	
//	saveNext[3] = saveNext[0] + saveNext[1] + saveNext[2];
//	saveNext[3] = saveNext[3] + 1;
//	
//	//SEG- In case more than 155 signals are going to be recorded make the next signal name zero to avoid variable overflow. 
//	if(saveNext[3]== 254){
//		saveNext[3]=0;
//	}
//
//	//SEG-Write new file with the number found in numfile.txt 
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"new ppg_%d.txt\r",*(saveNext+3));
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}
//	
//	//SEG-Write new file with the number found in numfile.txt 
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"new ecg_%d.txt\r",*(saveNext+3));
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}
//	
//	
//	//SEG- Write to numfile the next number to be used during reset.
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"write numfile.txt 3\r");
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 60){
//		app_uart_get(rxBuffer);
//	}
//	
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"%d\r",*(saveNext+3));
//	send_msg((uint8_t*)txBuffer);
//	
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"\n\r");
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}
//
//	
//	for(uint8_t j = 0; j<sizeof(rxOlBuffer);j++){
//		
//	}
//	return *(saveNext+ 3);
//}



//uint8_t init_OL_wAppend(void){
//	
//	uint8_t rxOlBuffer[100]={0}; //SEG - Buffer to recieve the incoming data from the OpenLog Module.
// 	uint8_t saveNext[4]={0};//SEG - Variable to store the 3 digit number to be written in the SD card.
//
//	//Reset OpenLog before start.
//	nrf_gpio_cfg_output(7); //SEG- Declare pin 0.07 as output.
//	nrf_gpio_pin_set(7);
//	nrf_delay_ms(100);
//	nrf_gpio_pin_clear(7);
//	nrf_delay_ms(100);
//	nrf_gpio_pin_set(7);
//	
//	//SEG- Wait until less than (<) is recieved signaling system reset.
//	while(rxBuffer[0] != 60){
//		app_uart_get(rxBuffer);
//	}
//	
//	//SEG- Put the OLog in command mode sending 3 times the 26 character. 
//	for(uint8_t i = 0; i<3; i++){
//		app_uart_put(26);
//	}
//	
//	//SEG-Waint until a grater than(>) is recieved in the rxBuffer.
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}	
//	
//	//app_uart_flush(); 
//	//SEG-Read numfile.txt to know which new file to create. 
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"read numfile.txt\r");
//	send_msg((uint8_t*)txBuffer);
//	get_msg(rxOlBuffer, 62);
//	
//	saveNext[0]= (rxOlBuffer[22]-48)*100;
//	saveNext[1]= (rxOlBuffer[23]-48)*10;
//	saveNext[2]= (rxOlBuffer[24]-48);
//	
//	saveNext[3] = saveNext[0] + saveNext[1] + saveNext[2];
//	saveNext[3] = saveNext[3] + 1;
//	
//	//SEG- In case more than 155 signals are going to be recorded make the next signal name zero to avoid variable overflow. 
//	if(saveNext[3]== 254){
//		saveNext[3]=0;
//	}
//
//	//SEG-Write new file with the number found in numfile.txt 
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"new sig_%d.txt\r",*(saveNext+3));
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}
//	
//	//SEG- Write to numfile the next number to be used during reset.
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"write numfile.txt 3\r");
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 60){
//		app_uart_get(rxBuffer);
//	}
//	
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"%d\r",*(saveNext+3));
//	send_msg((uint8_t*)txBuffer);
//	
//	memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"\n\r");
//	send_msg((uint8_t*)txBuffer);
//	
//	while(rxBuffer[0] != 62){
//		app_uart_get(rxBuffer);
//	}
//memset(txBuffer,0,sizeof(txBuffer));
//	sprintf(txBuffer,"append sig_%d.txt\r",*(saveNext+ 3));
//	send_msg((uint8_t*)txBuffer);
//	
//while(rxBuffer[0] != 60){//SEG - Wait for a less than '<' to start appending data. 
//		app_uart_get(rxBuffer);
//	}
//
//	return *(saveNext+ 3);
//}


void appendDataADS1292R(uint32_t* timeStamp,uint32_t* ecgData){
	
	//SEG - ECG data is a pointer to a uint32_t array of three positions. 
	memset(txBuffer,0,sizeof(txBuffer));
	sprintf(txBuffer,"\n;k;%x;%x\r\n",*timeStamp,*(ecgData+2));
	//send_msg((uint8_t*)txBuffer);
	
	return;
}


void appendADSBuffer(uint32_t* adsDataBufferPntr){

//SEG - Send analog ecg data aquired from an analog input pin.  
	memset(txBuffer,0,sizeof(txBuffer));
	sprintf(txBuffer,"\n;k;%x;%x;%x;%x;%x;%x;%x;%x;%x;%x\r\n",*(adsDataBufferPntr+0),*(adsDataBufferPntr+1),*(adsDataBufferPntr+2),*(adsDataBufferPntr+3),*(adsDataBufferPntr+4),*(adsDataBufferPntr+5),*(adsDataBufferPntr+6),*(adsDataBufferPntr+7),*(adsDataBufferPntr+8),*(adsDataBufferPntr+9));
        //send_msg((uint8_t*)txBuffer);
}



/***********************************************************
                    END - OpenLog Functions SEG
***********************************************************/

/*********************************************************
		ADS1292R Functions
**********************************************************/
uint32_t ADS1292_init(nrf_drv_spi_t spi){

	ret_code_t err_code;
	
	//SEG-Set GPIOs as Outputs. 
	nrf_gpio_cfg_output(clkSel);
	nrf_gpio_cfg_output(pwrDown);
	nrf_gpio_cfg_output(adsStart);
	
	
	nrf_gpio_pin_clear(adsStart);
	//SEG - Set CLK pin to high to signal the ADS that the clock is going to be interanl.
	nrf_gpio_pin_set(pwrDown);
	nrf_gpio_pin_set(clkSel); //SEG - Set clock select to high. Enable external clock.
	
        
        nrf_delay_ms(1000);//SEG - OOOJJJIOOOOOO Cambiar por un for y un NOP para evitar problemas con BLE y el blocking de esta function. 
	//for(uint16_t i=0; i<=50000; i++){
	//CAT - DO NOTHING. SETTLE WITHOUT BLE INTERRUPT. 
	//}


	//SEG-Reset Sequence.
	nrf_gpio_pin_clear(pwrDown);
	for(uint16_t i=0; i<=50000; i++){
	//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
	}
	nrf_gpio_pin_set(pwrDown);

	//Send SDATAC stop data adq coninuously 
	uint8_t m_tx_buf[1] = {SDATAC};//SEG - Buffer used to send single commands 
	err_code = nrf_drv_spi_transfer(&spi, m_tx_buf, sizeof(m_tx_buf), NULL, NULL);
	
	//SEG-Set the PD_REFBUF bit to one and wait for the reference to settle. 
	uint8_t referenceSequence[3] = {(WREG | 0x02), 0x00, CONFIG2};
	//error = nrf_drv_spi_transfer(&spi, referenceSequence, sizeof(referenceSequence), NULL, NULL);
	
	err_code =	nrf_drv_spi_transfer(&spi, referenceSequence, sizeof(referenceSequence), NULL, NULL);
	
	for(uint16_t i=0; i<=50000; i++){
	//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
	}
	
	uint8_t rxArray[13]= {0};

	//SEG - Config all of the ADS registers at startup. 
	uint8_t initArray[13] = {(WREG|0x01), 0x0A, CONFIG1, CONFIG2, LOFF, CH1SET, CH2SET, RLD_SENS, LOFF_SENS, LOFF_STAT, RESP1, RESP2, GPIO};
	err_code = nrf_drv_spi_transfer(&spi, initArray, sizeof(initArray), rxArray, sizeof(rxArray));
	
	//SEG - Read all of the registers after initial setup. 
	uint8_t regArray[14] = {0};
	uint8_t readRegsSequence[14] = {(RREG|0x00), 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
																										0x00, 0x00, 0x00, 0x00, 0x00, 0x00};//SEG - Remember to add the additional NULL bytes to provide CLK signal to SPI.
	err_code = nrf_drv_spi_transfer(&spi, readRegsSequence, sizeof(readRegsSequence), regArray, sizeof(regArray));
	
	
	//Place Start pin high to begin converting samples. 
	nrf_gpio_pin_set(adsStart);
																										
		for(uint16_t i=0; i<=10; i++){
		//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
			for(uint16_t i=0; i<=50000; i++){
			//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
			}
		}
		
	//Send RDATAC stop data adq coninuously 
	m_tx_buf[0] = RDATAC;//SEG - Buffer used to send single commands 
	err_code = nrf_drv_spi_transfer(&spi, m_tx_buf, sizeof(m_tx_buf), NULL, NULL);
	
	return err_code;
}

uint32_t ADS1292_ReadData(nrf_drv_spi_t spi,uint32_t* adsData){
	
	ret_code_t err_code;
	
	uint8_t dummyData[9] = {0};
	uint8_t recievedData[9] = {0};
	uint32_t dH = 0;
	uint32_t dM = 0;
	uint32_t dL = 0;
	uint8_t offset = 0;
	err_code = nrf_drv_spi_transfer(&spi, dummyData, sizeof(dummyData), recievedData, sizeof(recievedData));

	for(uint8_t i = 0; i<3; i++){
		dH = *(recievedData + offset);
		dM = *(recievedData + offset + 1);
		dL = *(recievedData + offset + 2);
		*(adsData+i) = (dH<<16)|(dM<<8)|(dL);
		offset = offset + 3;
	}

		
	return err_code;
}

/*********************************************************
	END - ADS1292R Functions
**********************************************************/



