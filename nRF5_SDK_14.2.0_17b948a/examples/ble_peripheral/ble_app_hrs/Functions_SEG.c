
#include "Functions_SEG.h"

#include "app_uart.h"
#include "nrf_uart.h"
#include "nrf_uarte.h"
#include <stdio.h>
#include <string.h>
#include "bsp.h"
#include "nrf_delay.h"


#include "nrf_drv_spi.h"


uint8_t rxBuffer[255] = {0};
char txBuffer[511] = {0};


uint32_t ADS1292_init(nrf_drv_spi_t spi){

	ret_code_t err_code;
	
	//SEG-Set GPIOs as Outputs. 
	nrf_gpio_cfg_output(clkSel);
	nrf_gpio_cfg_output(pwrDown);
	nrf_gpio_cfg_output(adsStart);
	
	
	nrf_gpio_pin_clear(adsStart);
	//SEG - Set CLK pin to high to signal the ADS that the clock is going to be interanl.
	nrf_gpio_pin_set(pwrDown);
	nrf_gpio_pin_set(clkSel); //SEG - Set clock select to high. Enable external clock.
	
        
        nrf_delay_ms(1000);//SEG - OOOJJJIOOOOOO Cambiar por un for y un NOP para evitar problemas con BLE y el blocking de esta function. 
	//for(uint16_t i=0; i<=50000; i++){
	//CAT - DO NOTHING. SETTLE WITHOUT BLE INTERRUPT. 
	//}


	//SEG-Reset Sequence.
	nrf_gpio_pin_clear(pwrDown);
	for(uint16_t i=0; i<=50000; i++){
	//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
	}
	nrf_gpio_pin_set(pwrDown);

	//Send SDATAC stop data adq coninuously 
	uint8_t m_tx_buf[1] = {SDATAC};//SEG - Buffer used to send single commands 
	err_code = nrf_drv_spi_transfer(&spi, m_tx_buf, sizeof(m_tx_buf), NULL, NULL);
	
	//SEG-Set the PD_REFBUF bit to one and wait for the reference to settle. 
	uint8_t referenceSequence[3] = {(WREG | 0x02), 0x00, CONFIG2};
	
	err_code = nrf_drv_spi_transfer(&spi, referenceSequence, sizeof(referenceSequence), NULL, NULL);
	
	for(uint16_t i=0; i<=50000; i++){
	//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
	}
	
	uint8_t rxArray[13]= {0};

	//SEG - Config all of the ADS registers at startup. 
	uint8_t initArray[13] = {(WREG|0x01), 0x0A, CONFIG1, CONFIG2, LOFF, CH1SET, CH2SET, RLD_SENS, LOFF_SENS, LOFF_STAT, RESP1, RESP2, GPIO};
	err_code = nrf_drv_spi_transfer(&spi, initArray, sizeof(initArray), rxArray, sizeof(rxArray));
	
	//SEG - Read all of the registers after initial setup. 
	uint8_t regArray[14] = {0};
	uint8_t readRegsSequence[14] = {(RREG|0x00), 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
																										0x00, 0x00, 0x00, 0x00, 0x00, 0x00};//SEG - Remember to add the additional NULL bytes to provide CLK signal to SPI.
	err_code = nrf_drv_spi_transfer(&spi, readRegsSequence, sizeof(readRegsSequence), regArray, sizeof(regArray));
	
	
	//Place Start pin high to begin converting samples. 
	nrf_gpio_pin_set(adsStart);
																										
		for(uint16_t i=0; i<=10; i++){
		//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
			for(uint16_t i=0; i<=50000; i++){
			//DO NOTHING TO GIVE TIME TO ADS CLK TO SETTLE. 
			}
		}
		
	//Send RDATAC stop data adq coninuously 
	m_tx_buf[0] = RDATAC;//SEG - Buffer used to send single commands 
	err_code = nrf_drv_spi_transfer(&spi, m_tx_buf, sizeof(m_tx_buf), NULL, NULL);
	
	return err_code;
}

uint32_t ADS1292_ReadData(nrf_drv_spi_t spi,uint32_t* adsData){
	
	ret_code_t err_code;
	
	uint8_t dummyData[9] = {0};
	uint8_t recievedData[9] = {0};
	uint32_t dH = 0;
	uint32_t dM = 0;
	uint32_t dL = 0;
	uint8_t offset = 0;
	err_code = nrf_drv_spi_transfer(&spi, dummyData, sizeof(dummyData), recievedData, sizeof(recievedData));

	for(uint8_t i = 0; i<3; i++){
		dH = *(recievedData + offset);
		dM = *(recievedData + offset + 1);
		dL = *(recievedData + offset + 2);
		*(adsData+i) = (dH<<16)|(dM<<8)|(dL);
		offset = offset + 3;
	}

		
	return err_code;
}




