# nRF5 ble ecg acquisition  
Code
 for nRF52 devices that includes:  

## Common  
- time_sync : synchronization ble beacon notifications from external device to update all other device clocks to the same value.  

## Peripheral  
- ble_app_sync_ads : (working) ADS data acquisition. Timestamp of ecg values comes from bluetooth synchronized timers. Sends data using UART.  
- ble_app_sync_ads_ble : (working) adquiere señal con el ADS y la envía correctamente por BLE.
- ble_app_cus : (dropped) conexión y habilitación de notificaciones inestable. Envía utilizando el UUID del hrs pero con custom service.
- ble_app_cus_ads_v3 : (dropped) dejo de funcionar, saca error hvn 3401.
- ble_app_hrs : (dropped) no utiliza correctamente el ADS. Conecta perfecto y transmite.


## Central  
- ble_app_hrs_c : (ejemplo inicial) connects, subscribes and recieves HVX notifs from cus peer. Receives data and prints it using RTT Logger.
- ble_app_hrs_c_uart_v1 : (???) dejo de funcionar.
- ble_app_hrs_c_uart_v2 : (working) connects to peripheral, and prints HVX event data through UART module.